# Kubernetes Ansible role

This role handles;

  * Installing a container runtime, only containerd.io supported as of writing.
  * Installing kubelet, kubeadm and tools required for kubernetes.
  * Does not start any service.

# Yum versionlock is broken

Suggestion: Just override the list of packages installed from the playbook.

```yaml
roles:
  - role: kubernetes
    k8s_yum_packages:
      - kubelet-1.19.10-0
      - kubeadm-1.19.10-0
      - kubectl-1.19.10-0
```
